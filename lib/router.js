
Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound'
});

// More info: https://github.com/EventedMind/iron-router/issues/3
AccountController = RouteController.extend({
    verifyEmail: function () {
        Accounts.verifyEmail(this.params.token, function () {
            Router.go('/verified');
        });
    }
});

var requireLogin = function() {
    console.log('requireLogin ');
    
    /* Returns true if 
        1. user login
        2. user session not expired
        3. user email verified
    */
  if (!Meteor.userId()) {
      console.log('accessDenied ');
    this.render('accessDenied');
    this.layout('landingLayout');
  } 
  else 
  {
    var user = Meteor.user();
    var dt = moment();  
      var isOK = true;
      
      
      if (user !== undefined)
      {
        if(user.emails !== undefined && !user.emails[0].verified) 
        {
            isOK = false;          
        }
            else if (user.persodata !== null || user.persodata !== undefined)
            {
                
                var lastlogin = moment(user.persodata.lastlogin);
                console.log('lastlogin ' + lastlogin);
                var diffDt = dt.diff(lastlogin, 'minutes');
                console.log('requireLogin ' + diffDt);
                if (diffDt>60)
                {
                    isOK = false; 
                }
            }
      }
        
        if (isOK)
        {
            setTimeout(function()
            {
                $('.splash').css('display', 'none')
            },500);
            this.next();
        }
        else 
        {
            
            swal({ title: 'Session expired', 
                    text: 'Sorry ' + user.username + ', your session has expired. Please log in.', 
                    allowEscapeKey:false, 
                    closeOnCancel:false,  
                    closeOnConfirm: true,
                    type:'info'
                }, 
                function() {
                      
                                Meteor.logout();
                                Router.go('login');   
                }
            );   
        }    
  }
};

Router.route('/categories', {name: 'categoriesList'});

Router.map(function () {

    this.route('verifyEmail', {
        controller: 'AccountController',
        path: '/verify-email/:token',
        action: 'verifyEmail'
    });
/*
    this.route('verified', {
        path: '/verified',
        template: 'verified'
    });

    this.route('checkemail', {
        path: '/checkemail',
        template: 'checkemail'
    });*/
    /*
     this.route('resetpassword', {
        path: '/reset-password/:resetPasswordToken',
        onBeforeAction: function() {
            Accounts._resetPasswordToken = this.params.resetPasswordToken;
            this.next();
        },
        template: 'blankLayout'
     });*/
});

Router.route('/checkemail', function () {
   
     this.render('checkemail');
        this.layout('blankLayout');
});

Router.route('/verified', function () {
   
     this.render('verified');
        this.layout('blankLayout');
});


//
// Dashboard route
//

Router.route('/', function () {
    
    var user = Meteor.user();
    var dt = moment();  
    if (!Meteor.userId() || user == undefined) {
            Router.go('login'); 
    }
    
    Router.go('dashboard');
    this.layout('layout');
},
{
    name:'home'
});

Router.route('/dashboard', function () {
   
     this.render('dashboard');
        this.layout('layout');
});

Router.route('/forgotpassword', function () {
   
     this.render('ForgotPassword');
        this.layout('blankLayout');
}),
{
    name: 'forgotpassword'
};

Router.route('/reset-password/:token', 
{
    name: 'resetpassword',
    layoutTemplate: 'blankLayout',
    onBeforeAction: function() {
        console.log('/reset-password/:resetPasswordToken '+ this.params.token);
            Accounts._resetPasswordToken = this.params.token;
            Session.set('resetPassword', Accounts._resetPasswordToken);
            this.next();
        }
});
/*
Router.onBeforeAction(IR_BeforeHooks.isLoggedIn, {only: ['resetpassword']});
*/

//
// Settings
//

Router.route('/settings', function () {
    this.render('settings');
});

Router.route('/profile', function () {
    this.render('profile');
});

//
// Workouts
//
Router.route('/workouts', function () {
    this.render('workouts');
});

//
// Records
//
Router.route('/records', function () {
    this.render('records');
});


//
// Common views route
//

Router.route('/login', function () {
    this.render('login');
    this.layout('blankLayout');
},
{
    name:'login'
});

Router.route('/termsOfUse', function () {
    this.render('termsOfUse');
    this.layout('blankLayout');
},
{
    name:'termsOfUse'
});
Router.route('/register', function () {
    this.render('register');
    this.layout('blankLayout');
},
{
    name:'register'
});
Router.route('/errorOne', function () {
    this.render('errorOne');
    this.layout('blankLayout');
});
Router.route('/errorTwo', function () {
    this.render('errorTwo');
    this.layout('blankLayout');
});

//
// Widgets route
//

Router.route('/widgets', function () {
    this.render('widgets');
});

//
// Global - Remove splash screen after after rendered layout
//

Router.onBeforeAction(requireLogin, {except: ['resetpassword', 'forgotpassword', 'home', 'verified', 'checkemail', 'login', 'register', 'verifyEmail', 'termsOfUse']});
