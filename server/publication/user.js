

Meteor.publish("userData", function() {
    // current user
    return Meteor.users.find({}, {fields: {'persodata': 1, 'workouts': 1}});
});

