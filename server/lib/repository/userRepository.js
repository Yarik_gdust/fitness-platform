
/***
 * User Repository management
 */

Meteor.methods({
    "userExists": function(username){
            return !!Meteor.users.findOne({username: username});
        },
    register: function (user) {
        this.unblock();
            //console.log('register server ' + user.username + ' ' + user.password + ' ' + user.identity);
            //console.log('register identity 0 '+ user.identity);
        try {
            
            check(user.username, String);
            
            // make sure the username not exits!
            if (Meteor.users.findOne({username: user.username}))
            {
                return {err:'Username already exists!', status:false};
            }
            
            var userId = Accounts.createUser({
                email: user.email,
                username: user.username,
                password: user.password,
                persodata: {
                            //'identity': user.identity !== undefined ? user.identity : null,
                            'lastlogin': new Date()
                        }
            });
               
            return {err:'', status:true, id:userId};
        } catch (e) {
            console.log('error ' + e);
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            return {err:e.message, status:false};
        }
    },
    SendVerifyEmail:function(email, userId) {
        this.unblock();
        
        var user = Meteor.users.findOne({_id: userId});
        if (!user)
        {
            console.log('Username does not exists!');
            return {err:'Username does not exists!', status:false};
        }
        
        if(user.emails[0].verified) 
        {
            console.log('User email already verified!');
            return {err:'User email already verified!', status:false};   
        }
        
        //var result = serverVerifyEmail(email, userId);
        
        var result = Meteor.call('serverVerifyEmail', email, userId);
            console.log('result:' + result);
        
        
        return result;
    },
    confirmEmail: function(userId) {
        Meteor.users.update({
            _id: userId
        },
        {
            $set: {
                    //'persodata.identity': new Date()
                    persodata: {
                        'identity': user.identity,
                        'lastlogin': new Date()
                    }
                }
        });
    },
    loginWithSocial: function() {
        console.log('loginWithSocial');
        if (!Meteor.userId)
        {
            return;
        }
        var user = Meteor.user();
        console.log('loginWithSocial 1 ' + user._id);
        Meteor.users.update({
            _id: user._id
        },
        {
            $set: {
                    'persodata.lastlogin': new Date()
                }
        });
        console.log('loginWithSocial 2');
         
    },
    loginuser: function(user) {
        console.log('login user function: ' + user.username + ' ' + user.password + ' ' + user.identity+ ' ' + user.email);
      
      try {
            this.unblock();
            
            var dtCurrent = new Date();   
            var puser = Meteor.users.findOne({username: user.username});
            
            if(!puser) {
                console.log('login failed');
                return {err:'Login has failed.', status:false};
            }
            
            var l_persodata = puser.persodata;
            l_persodata.lastlogin = dtCurrent;
            
            //creating the token and adding to the user
            var stampedToken = Accounts._generateStampedLoginToken();
            //hashing is something added with Meteor 0.7.x, 
            //you don't need to do hashing in previous versions
            var hashStampedToken = Accounts._hashStampedToken(stampedToken);
            
            Meteor.users.update(
                { _id: puser._id }, 
                { $set:{
                    //persodata: l_persodata
                    "persodata.lastlogin": l_persodata.lastlogin
                   /* persodata: {
                    'identity': l_persodata.identity,
                     lastlogin: l_persodata.lastlogin
                    }*/
                }}/*,
                { multi:true }*/
               );
               
            console.log('update ok ');
            return {status:true, token:hashStampedToken};    
      }  catch (e) {
          console.log('login catch error ' + e);
          return {err:e.message, status:false};
      }
    },
    logoutuser: function(user) {
      this.unblock();
      
      try {
          console.log('logout puser ' + user);
          var puser = Meteor.users.findOne({username: user.username});
            
          
            return {status:true};   
      }  catch (e) {
          console.log('logout catch error ' + e);
            var error = {message:e.message, status:false};
            console.log('server error message '+ error.message);
            console.log('server status '+ error.status);
            return error;
      }
    },
    changeUserPassword: function(user, password) {
      check(password, String);
      try {
          
            // make sure the username not exits!
            if (!Meteor.userId)
            {
                return {err:'You must be logged in!', status:false};
            }          
            console.log('user._id ' + user._id);
            console.log('user.password ' + user.password);
            console.log('user.password ' + password);
          Accounts.changePassword(user.password, password);
          
            return {status:true, err:''};   
      }  catch (e) {
            var error = {err:e.message, status:false};
            return error;
      }
    },
    getUserToken: function(user) {
         var puser = Meteor.users.findOne({username: user.username});
         var token = puser.services.resume.loginTokens[0].hashedToken;
         return token;
    },
    getProfile: function() {
        
        this.unblock();
        try {
            var id = Meteor.userId();
            var puser = Meteor.users.findOne( {_id:id});
       
            var username = "";
            var email = "";
            var gender = "";
            var lname = "";
            var fname = "";
            if (puser.emails) {
                email = puser.emails[0].address;
                username = puser.username;
            }
            else if (puser.services) {
                if (puser.services.facebook) {
                    email = puser.services.facebook.email;
                    gender = puser.services.facebook.gender;    
                }
                else if (puser.services.google) {
                    email = puser.services.google.email;
                }
                else if (puser.services.twitter) {
                    email = puser.services.twitter.email;
                }
            }
            
            if (puser.persodata) {
                if (!gender) {
                    gender = puser.persodata.gender;
                }
                if (!email) {
                    email = puser.persodata.email;
                }
                if (!lname) {
                    lname = puser.persodata.lname;
                    fname = puser.persodata.fname;
                }
                if (!fname) {
                    fname = puser.persodata.fname;
                }
            }
            
            var profile = {
              username: username,
              fname: fname,
              lname: lname,
              gender: gender,
              email: email,
              dob: puser.persodata.dob,
              weight: puser.persodata.weight,
              height: puser.persodata.height,
              /*country: puser.persodata.country,
              address1: puser.persodata.address1,
              address2: puser.persodata.address2,
              city: puser.persodata.city,
              province: puser.persodata.province,
              postcode: puser.persodata.postcode,
              phone: puser.persodata.phone,*/
            };
            
            return { status:true, profile: profile};
        }catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            console.log('server error message '+ e.message);
            console.log('server status '+ e.status);
            return { status:false, persodata:null, error:e.message};
        }
    },
    createOrUpdateProviderProfile: function(persodata) {
        this.unblock();
        try {
            
            var id = Meteor.userId();
           if (!id) {
               return {err:'The user must be logged in!', status:false};
           }
           
            Meteor.users.update({
                _id: id,
                }, {
                $set: {
                    persodata: {
                        'identity': persodata.identity,
                        'lastlogin': persodata.lastlogin,
                        //'uniquepatientid':persodata.uniquepatientid,
                        email:persodata.email,
                        fname:persodata.fname,
                        lname:persodata.lname,
                        surname:persodata.surname,
                        gender:persodata.gender,
                        dob:persodata.dob,
                        weight:persodata.weight,
                        height:persodata.height,
                        /*jobtitle:persodata.jobtitle,
                        mobile:persodata.mobile,
                        phone:persodata.phone,
                        address1:persodata.address1,
                        address2:persodata.address2,
                        area:persodata.area,
                        city:persodata.city,
                        province:persodata.province,
                        postcode:persodata.postcode,
                        country:persodata.country,
                        intro:persodata.intro,
                        avatar:persodata.avatar,*/
                    }
                }
            });
            
            return {status:true};    
            
        }
        catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.v
            console.log('server error message '+ e.message);
            return {err:e.message, status:false};
        }
    },
    createOrUpdateWorkouts: function(workout) {
        this.unblock();
        try {
            var id = Meteor.userId();
           if (!id) {
               return {err:'The user must be logged in!', status:false};
           }
           
           var existingWorkout = Meteor.users.findOne({workouts:{$elemMatch: {
                     name: workout.name,
                     category: workout.category
                }}});    
           
            if (existingWorkout)
            {
                console.log('workout already exists ');
                return  {err:"workout already exists", status:false, code:1};   
            }
            
            if (!workout.id)
            {
                // add the new workout
                Meteor.users.update(
                { _id:id},
                { $push: {
                            workouts:{
                                id: Random.id(),
                                name:workout.name,
                                category: workout.category
                            }
                        }
                    }
                );    
            }
            else
            {
                // add the new workout
                Meteor.users.update(
                { _id:id, 'workouts.id': workout.id },
                { $set: {
                            "workouts.$":{
                                id: workout.id, 
                                name:workout.name,
                                category: workout.category
                            }
                        }
                    }
                );
            }
           
            return {err:"", status:true, code:0};
        } catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            return {err:e.message, status:false, code:-1};
        }
    },
    removeWorkout: function(workout) {
        this.unblock();
        try {
        
            var id = Meteor.userId();
            if (!id) {
                return {err:'The user must be logged in!', status:false};
            }
            var id = Meteor.user()._id;
            // add the new workout
            Meteor.users.update(
            { _id:id, 'workouts.id': workout.id },
            { $pull: {
                        workouts:{
                            id: workout.id
                        }
                    }
                }
            );
            return {err:"", status:true, code:0};
        } catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            return {err:e.message, status:false, code:-1};
        }
    },
    createRecord:function(record) {
        this.unblock();
        try {
           var id = Meteor.userId();
           if (!id) {
               return {err:'The user must be logged in!', status:false};
           }
           
           // add the new workout
           Meteor.users.update(
           { _id:id},
           { $push: {
                records:{
                    id: Random.id(),
                    datetime: record.datetime,
                    duration: record.duration,
                    distance: record.distance,
                    heartRate: record.heartRate,
                    calories: record.calories,
                    notes: record.notes
                }
           }
           });    
        
           return {err:"", status:true, code:0};
        } catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            return {err:e.message, status:false, code:-1};
        }
    },
    deleteRecord: function(record) {
        this.unblock();
        try {
           var id = Meteor.userId();
           if (!id) {
               return {err:'The user must be logged in!', status:false};
           }
           
            var id = Meteor.user()._id;
            // remove the record
            Meteor.users.update(
            { _id:id, 'records.id': record.id },
            { $pull: {
                        records:{
                            id: record.id
                        }
                    }
                }
            );
           
           return {err:"", status:true, code:0};
        } catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            return {err:e.message, status:false, code:-1};
        }
        
    }
});