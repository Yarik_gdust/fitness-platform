Template.pUser.onRendered(function(){


    getPatientProfile();
    /*
    $("#birthday").birthdayPicker({
					"defaultDate":  user.persodata == undefined || user.persodata.dob == undefined ? moment() : user.persodata.dob,
                    placeholder: true,
					"maxAge": 100,
					"monthFormat":"long",
					"sizeClass": "span3"
				}); */
    
    $("#pers-info").validate({
      rules: {
           "birthday_birth[year]": {
               required: true,
               notEqualTo: "0"
           },
           "birthday_birth[month]": {
               notEqualTo: "0"
           },
           "birthday_birth[day]": {
               notEqualTo: "0"
           }
       }, /*
        messages: {
            "birthday_birth[month]": {
                notEqual: "Please enter a valid month."
            },
            "birthday_birth[year]": {
                notEqual: "Please enter a valid year."
            },
            "birthday_birth[day]": {
                notEqual: "Please enter a valid day."
            }
        },*/
        submitHandler: function(form) {
            console.log('call submitHandler');
            event.preventDefault();
            submitLoginForm(form);
        },
        errorPlacement: function(error, element) {
            $( element )
                .closest( "form" )
                .find( "label[for='" + element.attr( "id" ) + "']" )
                .append( error );
        },
        errorElement: "span",
    });
    
    $('#confidential-info').validate({
        rules: {
            password: {
                required: true,
                minlength: 8,
                equalTo: '#password'
            },
        },
        messages: {
            password: {
                required: "(Please enter your password)",
                minlength: "The mininum length for password is 8 characters"
            },
        },
        submitHandler: function(form) {
            console.log('call submitHandler');
            event.preventDefault();
            submitConfidentialForm(form);
        },
    });
});

Template.pUser.helpers({
  user: function() {
     var user = Meteor.user();
     return user;
  }
});

function getPatientProfile()
{
    console.log('call getpersodata');
    
    Meteor.call('getProfile', function(err, data) {
        if(data.status == false) {
                //$( '#form-error' ).text();
                $( '#form-error' ).html( 'Error happened.' );
                $( '#form-error' ).show();
        }
        else {
            if (data.status)
            {
                var profile = data.profile;
                var email = profile.email;
                
                $('#username').val(profile.username);
                $('#firstname').val(profile.fname);
                //$('#maidenname').val(profile.maidenname);
                $('#lastname').val(profile.lname);
                //$('#jobtitle').val(profile.jobtitle);
                if (profile.gender == "M" || profile.gender == "male")
                {
                $('#male').attr('checked', 'checked'); 
                }
                else 
                {
                $('#female').attr('checked', 'checked'); 
                }/*
                $('select#address-country option[value="'+profile.country+'"]').prop('selected', true);
                $('#address1').val(profile.address1);
                $('#address2').val(profile.address2);
                $('#address-city').val(profile.city);
                $('#address-area').val(data.province);
                $('#address-zip').val(profile.postcode);
                $('#phone').val(profile.phone);*/
                $('#email').val(profile.email);
                
                if (profile.dob!= undefined && profile.dob.length > 0)
                {
                    var dateOfBirth = profile.dob.split("-");
                    console.log('dateOfBirth ' + dateOfBirth + ' - ' + parseInt(dateOfBirth[1]) );
                    
                    var month = parseInt(dateOfBirth[1]) - 1;
                    var day = parseInt(dateOfBirth[2]) - 1;
                    var year = parseInt(dateOfBirth[0]) ;
                    
                    $("#birthday").birthdayPicker({
                        "defaultDate":  profile == undefined || profile.dob == undefined ? moment() : profile.dob,
                        placeholder: true,
                        "maxAge": 100,
                        "monthFormat":"long",
                        "sizeClass": "span3"
                    }); 
                }
            }   
        }
    });
}

function submitLoginForm(form)
{
    console.log('call submitLoginForm');
     var persodata = {
                    //uniquepatientid: form['uniquepatientid'].value,
                    "fname": form['firstname'].value,
                    //"maidenname": form['maidenname'] ? form['maidenname'].value : '',
                    "lname": form['lastname'] ? form['lastname'].value : '',
                    "weight": form['weight'] ? form['weight'].value : '',
                    "height": form['height'] ? form['height'].value : '',
                    //"jobtitle": '',
                    "gender": $('input[name=gender]:radio:checked').val(),
                    "dob": form['birthday_birth[year]'].value + '-' + form['birthday_birth[month]'].value + '-' + form['birthday_birth[day]'].value,
                    //"fileName": form['fileName'] ? form['fileName'].value : '',
                    /*"address1": form['address1'] ? form['address1'].value : '',
                    "address2": form['address2'] ? form['address2'].value : '',
                    "province": form['address-area'] ? form['address-area'].value : '',
                    "city": form['address-city'] ? form['address-city'].value : '',
                    "postcode": form['address-zip'] ? form['address-zip'].value : '',
                    "country": form['address[country]'] ? form['address[country]'].value : '',
                    "phone": form['phone'] ? form['phone'].value : '',*/
                    "email": form['email'] ? form['email'].value : '',
                };
             
    Meteor.call('createOrUpdateProviderProfile', persodata, function(err, data) {
        if(data.status == false) {
            
             swal({ title: 'Some error occured', 
                            text: data.err, 
                            allowEscapeKey:false, 
                            closeOnCancel:false,  
                            closeOnConfirm: true,
                            type:'error'
                          });
            
            if (data.err.indexOf('Token has expired')!=-1)
            {
                Router.go('login');
            }
            
            console.log('create persodata KO ' + data.err);
                //$( '#form-error' ).text();
                $( '#form-error' ).html( 'Error happened.' );
                $( '#form-error' ).show();
        }
        else {
            console.log('create OK - status' + data.status);
                swal({ title: 'Create succeeded', 
                    text: 'Your personal data has been modified.',
                    allowEscapeKey:false, 
                    closeOnCancel:false,  
                    closeOnConfirm: true,
                    type:'info'
                });
            
        }
            $(this).css({'cursor' : 'default'});
    });
}


function submitConfidentialForm(form) {
    'use strict';
    
    var password = form['password'].value;
    var user = Meteor.user();
    
    
    Meteor.call('changeUserPassword', user, password, function(err, data) {
        if(data.status == false) {
             swal({ title: 'Some error occured', 
                            text: data.err, 
                            allowEscapeKey:false, 
                            closeOnCancel:false,  
                            closeOnConfirm: true,
                            type:'error'
                          });
        }
        else{
              swal({ title: 'Create succeeded', 
                    text: 'Your password has been changed.',
                    allowEscapeKey:false, 
                    closeOnCancel:false,  
                    closeOnConfirm: true,
                    type:'info'
                });
            
        }
    });
}